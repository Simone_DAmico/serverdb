"""
Modulo per la generalizzazione dei campi per SQLite e MySQL, contenente le strutture dati al cui interno sono
incapsulate le chiamate alle funzioni di __init__ delle super classi.
"""

from __future__ import unicode_literals

from django.db import models


def FromGenToDjango(attr):
    if 'notNull' in attr:
        attr['null'] = not attr.pop('notNull')
    return attr


def FromGenToDjangoMany(attr): # relation_type, cls_name, direction, cardinality, model
    if 'model' in attr:
        attr['through'] = attr.pop('model')
    if 'direction' in attr:
        attr.pop('direction')
    if 'label' in attr:
        attr.pop('label')
    if 'cardinalitySource' in attr:
        attr.pop('cardinalitySource')
    if 'cardinalityTarget' in attr:
        attr.pop('cardinalityTarget')
    return attr

def FromGenToDjangoOne(attr): # model, relation_type, direction, cardinality
    if 'model' in attr:
        attr.pop('model')
    if 'direction' in attr:
        attr.pop('direction')
    if 'label' in attr:
        attr.pop('label')
    if 'cardinalitySource' in attr:
        attr.pop('cardinalitySource')
    if 'cardinalityTarget' in attr:
        attr.pop('cardinalityTarget')
    # attr['on_delete'] = 'models.CASCADE'
    return attr


# Classi per i Field types
class MyIDField(models.AutoField):
    def __init__(self, **kwargs):
        super(MyIDField, self).__init__(**kwargs)


class MyBooleanField(models.BooleanField):
    def __init__(self, *args, **kwargs):
        kwargs = FromGenToDjango(kwargs)
        super(MyBooleanField, self).__init__(*args, **kwargs)


class MyStringField(models.CharField):
    def __init__(self, *args, **kwargs):
        kwargs = FromGenToDjango(kwargs)
        kwargs['max_length'] = 255
        super(MyStringField, self).__init__(*args, **kwargs)


class MyDateField(models.DateField):
    def __init__(self, *args, **kwargs):
        kwargs = FromGenToDjango(kwargs)
        super(MyDateField, self).__init__(*args, **kwargs)


class MyFloatField(models.FloatField):
    def __init__(self, *args, **kwargs):
        kwargs = FromGenToDjango(kwargs)
        super(MyFloatField, self).__init__(*args, **kwargs)


class MyIntegerField(models.IntegerField):
    def __init__(self, *args, **kwargs):
        kwargs = FromGenToDjango(kwargs)
        super(MyIntegerField, self).__init__(*args, **kwargs)


# Classi per i Relationship types
class MyManyToOneField(models.ForeignKey):
    def __init__(self, to, **kwargs):
        kwargs = FromGenToDjangoOne(kwargs)
        super(MyManyToOneField, self).__init__(to, **kwargs)


class MyOneToOneField(models.OneToOneField):
    def __init__(self, to, **kwargs):
        kwargs = FromGenToDjangoOne(kwargs)
        super(MyOneToOneField, self).__init__(to, **kwargs)


class MyManyToManyField(models.ManyToManyField):
    def __init__(self, to, **kwargs):
        kwargs = FromGenToDjangoMany(kwargs)
        super(MyManyToManyField, self).__init__(to, **kwargs)


# Classe per i nodi
class MyNode(models.Model):
    def __init__(self, *args, **kwargs):
        super(MyNode, self).__init__(*args, **kwargs)

    @classmethod
    def getNodes(cls):
        """
        Metodo per ottenere tutti i nodi di cls
        :return: QuerySet
        """
        return cls.objects.all()

    @classmethod
    def countNodes(cls):
        """
        Metodo per contare tutti i nodi di cls
        :return: Integer
        """
        return cls.objects.count()

    @classmethod
    def orderByParam(cls, param):
        """
        Metodo per ordinare i nodi della classe cls
        :param parametro: criterio di ordinamento
        :return: QuerySet
        """
        return cls.objects.order_by(param)

    @classmethod
    def getSubSetNodes(cls, offset, limit):
        """
        Metodo per ottenere un sotto insieme dei nodi della classe cls
        :param offset: posizione del primo nodo considerato (inizia da 0)
        :param limit: posizione dell'ultimo nodo considerato
        :return: QuerySet
        """
        return cls.getNodes()[offset:limit]

    @classmethod
    def getDistinctNodes(cls, *args):
        """
        Metodo per ottenere i nodi distinti della classe cls
        :param args: parametri su cui eliminare i diplicati
        :return: QuerySet
        """
        return cls.getNodes().distinct(*args)


# Classe per gli archi
class MyEdge(models.Model):
    def __init__(self, *args, **kwargs):
        super(MyEdge, self).__init__(*args, **kwargs)

    @classmethod
    def getEdges(cls):
        """
        Metodo per ottenere tutti gli archi di cls
        :return: QuerySet
        """
        return cls.objects.all()

    @classmethod
    def countEdges(cls):
        """
        Metodo per contare tutti gli archi di cls
        :return: Integer
        """
        return cls.objects.count()

    @classmethod
    def orderByParam(cls, param):
        """
        Metodo per ordinare gli archi della classe cls
        :param parametro: criterio di ordinamento
        :return: QuerySet
        """
        return cls.objects.order_by(param)
