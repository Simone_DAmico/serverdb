app
/**
 * Controller per il salvataggio del progetto corrente
 * @module saveProject
 * @param {Object} $scope L'oggetto {@link https://docs.angularjs.org/guide/scope|$scope} indica il contesto in cui sono
 * salvati i dati e valutate le espressioni
 * @param {Object} $http Il servizio AngularJS {@link https://docs.angularjs.org/api/ng/service/$http|$http} per la
 * comunicazione con i server HTTP
 * @param {Object} $mdDialog Il servizio di AngularJS Material
 * {@link https://material.angularjs.org/1.1.6/api/service/$mdDialog|$mdDialog} permette di gestire una finestra di
 * dialogo con l'utente
 * @param {Object} Labels Il servizio {@link Labels} contiene i dati del grafo utente
 */
.controller("saveProject", function ($scope, $http, $mdDialog, Labels) {

    /**
     * Salva i dati inviandoli alla view create_schema_NEO4J o alla view create_schema_SQL a seconda del DBMS
     * @function salvataggio
     */
    $scope.salvataggio = function() {
        $scope.dbms = Labels.dbms;
	    console.log($scope.dbms);
	    if ($scope.dbms === 'Neo4j')
	        $http.post("../create_schema_NEO4J/", JSON.stringify(Labels))
                .then(function successCallback(response){
                    console.log("Successfully POST-ed data to create_schema_NEO4J");
                    }, function errorCallback(response){
                    console.log("POST-ing of data failed to create_schema_NEO4J");
                });
	    else
            $http.post("../create_schema_SQL/", JSON.stringify(Labels))
                .then(function successCallback(response){
                    console.log("Successfully POST-ed data to create_schema_SQL");
                    }, function errorCallback(response){
                    console.log("POST-ing of data failed to create_schema_SQL");
                });
	};
});