"""
Modulo per la generalizzazione dei campi per Neo4J, contenente le strutture dati al cui interno sono
incapsulate le chiamate alle funzioni di __init__ delle super classi.
"""

from __future__ import unicode_literals

from neomodel import *
# primary_key, unique, default, notNull, choices


def fromGenToNeomodel(attr):
    if 'primary_key' in attr:
        attr['unique_index'] = attr.pop('primary_key')
    if attr[attr.keys()[0]] is None:
        attr.pop('default')
    if attr[attr.keys()[0]] is not None:
        attr.pop('notNull')
    if 'notNull' in attr:
        attr['required'] = attr.pop('notNull')
    return attr


def FromGenToDjangoRel(attr): # class, model, label, cardinalitySource, cardinalityTarget, direction
    if 'label' in attr:
        attr.pop('label')
    if 'cardinalitySource' in attr:
        attr.pop('cardinalitySource')
    if 'cardinalityTarget' in attr:
        attr.pop('cardinalityTarget')
    return attr


# Classi per i Property types
class MyIDField(UniqueIdProperty):
    def __init__(self, **kwargs):
        super(MyIDField, self).__init__(**kwargs)


class MyBooleanField(BooleanProperty):
    def __init__(self, **kwargs):
        kwargs = fromGenToNeomodel(kwargs)
        super(MyBooleanField, self).__init__(**kwargs)


class MyStringField(StringProperty):
    def __init__(self, *args, **kwargs):
        kwargs = fromGenToNeomodel(kwargs)
        super(MyStringField, self).__init__(*args, **kwargs)


class MyDateField(DateProperty):
    def __init__(self, **kwargs):
        kwargs = fromGenToNeomodel(kwargs)
        super(MyDateField, self).__init__(**kwargs)


class MyFloatField(FloatProperty):
    def __init__(self, **kwargs):
        kwargs = fromGenToNeomodel(kwargs)
        super(MyFloatField, self).__init__(**kwargs)


class MyIntegerField(IntegerProperty):
    def __init__(self, **kwargs):
        kwargs = fromGenToNeomodel(kwargs)
        super(MyIntegerField, self).__init__(**kwargs)


# Classi per le Relationship type
class MyEdge(StructuredRel):
    def __init__(self, *args, **kwargs):
        super(MyEdge, self).__init__(*args, **kwargs)

    @classmethod
    def countEdges(cls):
        """
        Metodo per contare i nodi della classe cls
        :return: Intero
        """
        res = db.cypher_query('MATCH ()-->() RETURN count(*)')
        return res[0][0][0]


class MyManyToManyField(RelationshipDefinition):
    def __init__(self, cls_name, label, direction, cardinalitySource, cardinalityTarget, model=None):
        if cardinalitySource == '0 - N':
            cardinalitySource = 'ZeroOrMore'
        elif cardinalitySource == '1 - N':
            cardinalitySource = 'OneOrMore'
        super(MyManyToManyField, self).__init__(cls_name, label, direction)
        if direction == 'OUTGOING':
            RelationshipTo(cls_name, label, cardinalitySource, model)
        elif direction == 'INCOMING':
            RelationshipFrom(cls_name, label, cardinalitySource, model)


class MyOneToOneField(RelationshipDefinition):
    def __init__(self, cls_name, label, direction, cardinalitySource, cardinalityTarget, model=None):
        if cardinalitySource == '0 - 1':
            cardinalitySource = 'ZeroOrOne'
        elif cardinalitySource == '1 - 1':
            cardinalitySource = 'One'
        super(MyOneToOneField, self).__init__(label, cls_name, direction)
        if direction == 'OUTGOING':
            RelationshipTo(cls_name, label, cardinalitySource, model)
        elif direction == 'INCOMING':
            RelationshipFrom(cls_name, label, cardinalitySource, model)


class MyManyToOneField(RelationshipDefinition):
    def __init__(self, cls_name, label, direction, cardinalitySource, cardinalityTarget, model=None):
        if cardinalitySource == '0 - 1':
            cardinalitySource = 'ZeroOrOne'
        elif cardinalitySource == '1 - 1':
            cardinalitySource = 'One'
        elif cardinalitySource == '0 - N':
            cardinalitySource = 'ZeroOrMore'
        elif cardinalitySource == '1 - N':
            cardinalitySource = 'OneOrMore'
        super(MyManyToOneField, self).__init__(label, cls_name, direction)
        if direction == 'OUTGOING':
            RelationshipTo(cls_name, label, cardinalitySource, model)
        elif direction == 'INCOMING':
            RelationshipFrom(cls_name, label, cardinalitySource, model)


# Classi per i Node types
class MyNode(StructuredNode):
    def __init__(self, *args, **kwargs):
        super(MyNode, self).__init__(*args, **kwargs)

    @classmethod
    def getNodes(cls):
        """
        Metodo per ottenere tutti nodi della classe cls
        :return: NodeSet
        """
        return cls.nodes

    @classmethod
    def countNodes(cls):
        """
        Metodo per contare i nodi della classe cls
        :return: Intero
        """
        return len(cls.nodes)

    @classmethod
    def orderByParam(cls, parametro):
        """
        Metodo per ordinare i nodi della classe cls
        :param parametro: criterio di ordinamento
        :return: NodeSet
        """
        return cls.nodes.order_by(parametro)

    @classmethod
    def hasRel(cls, prop):
        """
        Metodo per ottenere i nodi di cls che sono coinvolti in una relationship tramite prop
        :param prop: proprieta' di cls che definisce una relationship
        :return: NodeSet
        """
        return cls.nodes.has(prop)
