from __future__ import unicode_literals
# Create your models here.
from MyStructuresSQL import *

from django.core.exceptions import ValidationError

class Casa(MyNode):
    id_casa = MyIDField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')
    mynode_ptr = MyOneToOneField(auto_created=True, on_delete=models.CASCADE, parent_link=True, to='app.MyNode', null = True)
    nomeProp1_1 = MyIntegerField(default=1, notNull=True)
    nomeProp1_2 = MyStringField(choices=(('Maschio', 'Maschio'), ('Femmina', 'Femmina')), default='Maschio', notNull=False)
    nomeProp1_3 = MyIntegerField(default=6, notNull=False)
    nomeProp1_4 = MyIntegerField(default=0, notNull=True)
    nomeProp1_5 = MyIntegerField(default=0, notNull=True)
    edge_link_1 = MyManyToManyField('Nodo2', model='Link_1', label='LINK_1', cardinalitySource='0 - N', cardinalityTarget='0 - N', direction='OUTGOING')
    edge_nodo3 = MyOneToOneField('Nodo3', model='link_2', label='LINK_2', cardinalitySource='0 - 1', cardinalityTarget='0 - 1', direction='OUTGOING')
    nomeProplink2_1 = MyIntegerField(default=5.64, notNull=True)
    nomeProplink2_2 = MyFloatField(default=5.88, notNull=True)
    nomeProplink3_1 = MyFloatField(default=10.10, notNull=True)

    class Meta:
        unique_together = ()


class Nodo2(MyNode):
    id_nodo2 = MyIDField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')
    mynode_ptr = MyOneToOneField(auto_created=True, on_delete=models.CASCADE, parent_link=True, to='app.MyNode', null = True)
    nomeProp2_1 = MyIntegerField(default=12, notNull=True)
    nomeProp2_2 = MyDateField(notNull=False)

    class Meta:
        unique_together = ()


class Nodo3(MyNode):
    id_nodo3 = MyIDField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')
    mynode_ptr = MyOneToOneField(auto_created=True, on_delete=models.CASCADE, parent_link=True, to='app.MyNode', null = True)

    class Meta:
        unique_together = ()


class Link_1(MyEdge):
    id_link_1 = MyIDField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')
    myedge_ptr = MyOneToOneField(auto_created=True, null=True, on_delete=models.CASCADE, parent_link=True, serialize=False, to='app.MyEdge')
    casa = MyManyToOneField('Casa', model='link_1', label='LINK_1', cardinalitySource='0 - N', cardinalityTarget='0 - N', direction='OUTGOING')
    nodo2 = MyManyToOneField('Nodo2', model='link_1', label='LINK_1', cardinalitySource='0 - N', cardinalityTarget='0 - N', direction='INCOMING')
    nomeProplink1_1 = MyIntegerField(default=12, notNull=True)
    nomeProplink1_2 = MyIntegerField(default=1552, notNull=True)

    class Meta:
        unique_together = ()




